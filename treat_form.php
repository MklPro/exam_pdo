<?php

if (isset($_POST["title"]) && isset($_POST["launchAt"]) && isset($_POST["imgLink"]) && isset($_POST["genre"])) {
        $title = $_POST["title"];
        $launchAt = $_POST["launchAt"];
        $imgLink = $_POST["imgLink"];
        $genre = $_POST["genre"];

        try {
                $pdo = new PDO("mysql:host=localhost;dbname=netfake", "root", "");
                $query = "INSERT INTO movie (title, launch_at, image_link, genre_id) VALUES (:title, :launchAt, :imgLink, :genre)";
                $resultats = $pdo->prepare($query);
                $resultats->execute([
                            ":title" => $title,
                            ":launchAt" => $launchAt,
                            ":imgLink" => $imgLink,
                            ":genre" => $genre

                ]);

                header("location: index.php");
                exit();

        } catch (Exception $e) {
                echo $e->getMessage();
        }
};