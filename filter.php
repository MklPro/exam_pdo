<?php

try {
        $pdo = new PDO('mysql:host=localhost;dbname=netfake', 'root', '');

        if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $query = "SELECT * FROM movie WHERE movie.title LIKE '%' :search '%'";

                $results = $pdo->prepare($query);
                $results->execute([
                            ':search' => $search
                ]);
        } else {
                $query = 'SELECT * FROM movie';
                $results = $pdo->prepare($query);
                $results->execute();
        }

        $rows = $results->fetchAll(PDO::FETCH_ASSOC);

} catch (Exception $e) {
        echo $e->getMessage();
}

function dump($data)
{
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
}

