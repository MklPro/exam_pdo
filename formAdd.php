<?php

try {
        $pdo = new PDO('mysql:host=localhost;dbname=netfake', 'root', '');

        $query = 'SELECT * FROM genre';
        $results = $pdo->prepare($query);
        $results->execute();

        $rows = $results->fetchAll(PDO::FETCH_ASSOC);

} catch (Exception $e) {
        var_dump($e);
}

function dump($data)
{
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="stylesheet" href="index.css">
</head>
<body>

<div class="container-fluid">
        <div class="row">
                <div class="col-12">

                        <form class="form" method="post" action="treat_form.php">
                                <div>
                                        <label for="title">Nom du film</label>
                                        <input type="text" name="title" id="title">
                                </div>

                                <div>
                                        <label for="launchAt">Date de sortie</label>
                                        <input type="date" name="launchAt" id="launchAt">
                                </div>

                                <div>
                                        <label for="imgLink">Lien vers l'image</label>
                                        <input type="text" name="imgLink" id="imgLink">
                                </div>

                                <div>
                                        <label for="genre">Genre</label>
                                        <select type="text" name="genre" id="genre">
                                                <?php foreach ($rows as $row) { ?>
                                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['label'] ?></option>
                                                <?php } ?>
                                        </select>
                                </div>

                                <div>
                                        <button class="btn btn-primary" type="submit">Ajouter un film</button>
                                        <a href="index.php">
                                                <button class="btn btn-primary" type="button">Retour à la
                                                        liste
                                                </button>
                                        </a>
                                </div>

                        </form>
                </div>
        </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>



