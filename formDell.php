<?php

if(isset($_GET['id'])) {
        $id = $_GET['id'];

        try {
                $pdo = new PDO("mysql:host=localhost;dbname=netfake","root","");
                $query = "DELETE FROM movie WHERE movie.id = :id";
                $resultats = $pdo->prepare($query);
                $resultats->execute([
                            ":id" => $id
                ]);

                header("location: index.php");
                exit();

        } catch (Exception $e) {
                echo $e->getMessage();
        }
}
