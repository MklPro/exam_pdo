Pour initialiser le projet sur votre machine :

    1. Clonez le projet sur votre machine.
    2. Faire un 'npm install' dans le terminal de votre IDE.
    3. Faire un 'npm install bootstrap' ensuite toujours dans le terminal.
    4. Copiez ce qui se trouve dans le ficher "bdd.txt" et coller le dans une requête SQL sur phpMyAdmin.
    5. Faire un "php -S localhost:8000" pour lancer le serveur dans le terminal de votre IDE
    6. Entrez l'adresse généré dans votre navigateur ou cliquez directement sur celui dans votre terminal.
    
    Vous pouvez désormais consulter la liste des films, ajouter/supprimer des films et faire une recherche dans l'index !
  
