<?php
 include 'filter.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="stylesheet" href="index.css">
</head>
<body>

<div class="container">
        <div class="row">
                <div class="col-12">
                        <h1 class="my-5"><a href="index.php">Index des films</a></h1>
                </div>
        </div>

        <div class="row">
                <div class="col-12">
                        <form action="" method="get">
                                <label for="search">Rechercher un film</label>
                                <input type="text" name="search" id="search">
                                <button class="btn btn-primary" type="submit">Rechercher</button>
                        </form>
                </div>
        </div>

        <div class="row">
                <?php
                foreach ($rows as $row) {
                        ?>

                        <div class="col-4">
                                <div class="card my-5" style="width: 18rem;">
                                        <div class="card-body">
                                                <h5 class="card-title"><?php echo $row["title"] ?></h5>

                                                <a href="detail.php?id=<?php echo $row['id'] ?>">
                                                        <button class="btn btn-primary" type="submit">Voir le
                                                                détail
                                                        </button>
                                                </a>

                                                <a href="formDell.php?id=<?php echo $row['id'] ?>">
                                                        <button class="btn btn-primary" type="submit">
                                                                Supprimer
                                                        </button>
                                                </a>
                                        </div>
                                </div>
                        </div>

                        <?php
                }
                ?>
        </div>

        <div class="row mt-5">
                <div class="col-12">
                        <a href="formAdd.php">
                                <button class="btn btn-primary" type="submit">Ajouter un film</button>
                        </a>
                </div>
        </div>

</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>







