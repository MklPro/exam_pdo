<?php

if (isset($_GET['id'])) {
        $id = $_GET['id'];

        try {
                $pdo = new PDO("mysql:host=localhost;dbname=netfake", "root", "");

                $query = "SELECT *, genre.label as genreName FROM movie JOIN genre ON movie.genre_id = genre.id WHERE movie.id =:id";
                $resultats = $pdo->prepare($query);
                $resultats->execute([
                            ":id" => $id
                ]);

                $rows = $resultats->fetch(PDO::FETCH_ASSOC);
                $date = new DateTime($rows['launch_at']);

        } catch (Exception $e) {
                echo $e->getMessage();
        }

} else {
        echo " je n'ai pas reçu d'id !";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="stylesheet" href="index.css">
</head>
<body>

<div class="container">

        <div class="row my-5">
                <div class="col-12">
                        <div class="card" style="width: 18rem;">
                                <img class="card-img-top"
                                     src="<?php echo $rows['image_link'] ?>"
                                     alt="Card image cap">
                                <div class="card-body">
                                        <h5 class="card-title"><?php echo $rows["title"] ?></h5>
                                        <p class="card-text">Date de sortie : <?php echo $date->format('d/m/Y') ?></p>
                                        <p class="card-text">Genre
                                                : <?php echo $rows["genreName"] ?></p>
                                </div>
                        </div>
                </div>
        </div>

        <div class="row">
                <div class="col-12">
                        <a href="index.php">
                                <button class="btn btn-primary" type="submit">Retour à la
                                        liste
                                </button>
                        </a>
                </div>
        </div>

</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>


